module API
  class ProtectedBranches < Grape::API
    include PaginationParams

    before { authorize! :download_code, user_project }

    params do
      requires :id, type: String, desc: 'The ID of a project'
    end
    resource :projects, requirements: { id: %r{[^/]+} } do
      helpers do
        def translate_permissions(access_level)
          case access_level
          when 'developers'
            ::Gitlab::Access::DEVELOPER
          when 'masters'
            ::Gitlab::Access::MASTER
          when 'no_one'
            ::Gitlab::Access::NO_ACCESS
          end
        end
      end

      desc 'Get a project protected branches' do
        success Entities::RepoProtectedBranch
      end
      params do
        use :pagination
      end
      get ':id/protected_branches' do
        protected_branches = user_project.protected_branches

        present paginate(protected_branches), with: Entities::RepoProtectedBranch, project: user_project
      end

      desc 'Get a single protected branch' do
        success Entities::RepoProtectedBranch
      end
      params do
        requires :branch, type: String, desc: 'The name of the branch or wildcard'
      end
      get ':id/protected_branches/:branch' do
        protected_branch = user_project.protected_branches.find_by(name: params[:branch])
        not_found!('Protected branch') unless protected_branch

        present protected_branch, with: Entities::RepoProtectedBranch, project: user_project
      end

      desc 'Protect a single branch or wildcard' do
        success Entities::RepoProtectedBranch
      end
      params do
        requires :branch, type: String, desc: 'The name of the protected branch'
        optional :push_access_level, type: String, values: %w[developers masters no_one],
                                     default: 'developers',
                                     desc: 'Lowest access level allowed to merge'
        optional :merge_access_level, type: String, values: %w[developers masters no_one],
                                      default: 'developers',
                                      desc: 'Lowest access level allowed to push'
      end
      put ':id/protected_branches/:branch/protect' do
        authorize_admin_project

        protected_branch = user_project.protected_branches.find_by(name: params[:branch])

        protected_branch_params = {
          name: params[:branch],
          push_access_levels_attributes: [{ access_level: translate_permissions(params[:push_access_level]) }],
          merge_access_levels_attributes: [{ access_level: translate_permissions(params[:merge_access_level]) }]
        }

        service_args = [user_project, current_user, protected_branch_params]

        protected_branch = if protected_branch
                             # should probably be in UpdateService
                             protected_branch.transaction do
                               protected_branch.merge_access_levels.destroy_all
                               protected_branch.push_access_levels.destroy_all
                               ::ProtectedBranches::UpdateService.new(*service_args).execute(protected_branch)
                             end
                           else
                             ::ProtectedBranches::CreateService.new(*service_args).execute
                           end
        if protected_branch.valid?
          present protected_branch, with: Entities::RepoProtectedBranch, project: user_project
        else
          render_api_error!(protected_branch.errors.full_messages, 422)
        end
      end

      desc 'Unprotect a single branch'
      params do
        requires :branch, type: String, desc: 'The name of the protected branch'
      end
      delete ':id/protected_branches/:branch/unprotect', requirements: { branch: /.+/ } do
        authorize_admin_project

        protected_branch = user_project.protected_branches.find_by(name: params[:branch])
        not_found!('Protected branch') unless protected_branch
        protected_branch&.destroy
      end
    end
  end
end